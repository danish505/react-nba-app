import React from 'react';
import style from './header.css';
import FontAwesome from 'react-fontawesome';
import {Link} from 'react-router-dom';

const Header = () =>{

    const logo = () =>
    (
        <Link to="/">
            <img src="/images/nba_logo.png" alt="NBA Logo"/>
        </Link>
    )

    const Navbar = () =>{
        return(
            <div className={style.bars}>
                <FontAwesome name="bars" Style={{color:'#fff',padding:'10px'}}/>
            </div>
        )
    }

    return(
        <header className={style.header}>
            <div className={style.headerOpt}>
            {logo()}
            </div>

        </header>
    )


}

export default Header;